terraform {
  backend "gcs" {
  }
}
provider "random" {
}
provider "google" {
  alias  = "init"
  region = var.region
}
provider "google" {
  region  = var.region
  project = data.google_projects.root.projects[0].project_id
}
provider "gitlab" {
  token = var.gitlab_token
}
