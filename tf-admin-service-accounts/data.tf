data "google_projects" "root" {
  provider = google.init
  filter   = "labels.id_prefix:${var.project_id} lifecycleState:ACTIVE"
}
data "gitlab_group" "vicpah" {
  full_path = "VicPAH"
}
data "gitlab_user" "vicpah_infra" {
  username = "vicpah_infra"
}
