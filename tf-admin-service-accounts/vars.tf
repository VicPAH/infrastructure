variable "project_id" {
  default = "vicpah-tf-admin"
}
variable "region" {
  default = "australia-southeast1"
}
variable "gitlab_token" {}
variable "google_org_id" {}
variable "google_billing_account" {}
