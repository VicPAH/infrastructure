terraform {
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 3.5"
    }
    google = {
      source  = "hashicorp/google"
      version = "~> 3.31"
    }
    google-beta = {
      source  = "hashicorp/google-beta"
      version = "~> 3.31"
    }
    random = {
      source  = "hashicorp/random"
      version = "~> 2.3"
    }
  }
  required_version = ">= 0.13"
}
