resource "google_service_account" "gitlab" {
  account_id   = "gitlab"
  display_name = "GitLab"
}
resource "google_service_account_key" "gitlab" {
  service_account_id = google_service_account.gitlab.name
}
resource "google_project_iam_member" "gitlab" {
  for_each = toset([
    "roles/viewer",
    "roles/storage.admin",
  ])
  member = "serviceAccount:${google_service_account.gitlab.email}"
  role   = each.value
}
resource "google_project_iam_member" "biru" {
  member = "user:biru@vicpah.org.au"
  role   = "roles/storage.admin"
}
resource "google_organization_iam_member" "gitlab" {
  for_each = toset([
    "roles/billing.admin",
    "roles/iam.securityAdmin",
    "roles/resourcemanager.folderAdmin",
    "roles/resourcemanager.projectCreator",
    "roles/resourcemanager.projectDeleter",

  ])
  member = "serviceAccount:${google_service_account.gitlab.email}"
  role   = each.value
  org_id = var.google_org_id
}

resource "gitlab_group_membership" "infra" {
  group_id     = data.gitlab_group.vicpah.id
  user_id      = data.gitlab_user.vicpah_infra.id
  access_level = "owner"
}
resource "gitlab_project" "infra" {
  name             = "Infrastructure"
  namespace_id     = data.gitlab_group.vicpah.id
  visibility_level = "public"
  default_branch   = "master"

  container_registry_enabled                       = false
  only_allow_merge_if_all_discussions_are_resolved = true
  only_allow_merge_if_pipeline_succeeds            = true
  shared_runners_enabled                           = true
  snippets_enabled                                 = false
  wiki_enabled                                     = false
}
resource "gitlab_branch_protection" "master" {
  project            = gitlab_project.infra.id
  branch             = "master"
  push_access_level  = "developer"
  merge_access_level = "developer"
}
resource "gitlab_project_variable" "google_sa_private" {
  project           = gitlab_project.infra.id
  key               = "GOOGLE_APPLICATION_CREDENTIALS"
  value             = base64decode(google_service_account_key.gitlab.private_key)
  variable_type     = "file"
  protected         = true
  environment_scope = "*"
}
resource "gitlab_project_variable" "google_sa_email" {
  project           = gitlab_project.infra.id
  key               = "TF_VAR_google_sa_email"
  value             = google_service_account.gitlab.email
  environment_scope = "*"
}


module "tfplan_pass" {
  source        = "../tf_modules/gitlab_secret_var"
  project       = gitlab_project.infra.id
  name          = "tfplan_pass"
  variable_type = "file"
}
