resource "google_billing_budget" "budget" {
  provider        = google-beta
  billing_account = var.google_billing_account
  display_name    = "$20AUD"
  amount {
    specified_amount {
      currency_code = "AUD"
      units         = "20"
    }
  }
  threshold_rules {
    threshold_percent = 0.5
  }
  threshold_rules {
    threshold_percent = 0.9
  }
  threshold_rules {
    threshold_percent = 1.0
  }
  threshold_rules {
    threshold_percent = 0.9
    spend_basis       = "FORECASTED_SPEND"
  }
}
