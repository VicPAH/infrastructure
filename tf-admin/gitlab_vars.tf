# Google
resource "gitlab_group_variable" "google_org_id" {
  group = var.gitlab_group_id
  key   = "TF_VAR_google_org_id"
  value = var.google_org_id
}
resource "gitlab_group_variable" "google_billing_account" {
  group = var.gitlab_group_id
  key   = "TF_VAR_google_billing_account"
  value = var.google_billing_account
}

# Terraform
resource "gitlab_group_variable" "tf_backend_bucket" {
  group = var.gitlab_group_id
  key   = "TF_VAR_tf_backend_bucket"
  value = google_storage_bucket.state_backend.name
}
