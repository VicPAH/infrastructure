provider "random" {
}
provider "google" {
  alias  = "init"
  region = var.google_region
}
provider "google" {
  region  = var.google_region
  project = google_project.project.project_id
}
provider "gitlab" {
  token = var.gitlab_token
}
