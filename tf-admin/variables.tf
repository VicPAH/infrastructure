variable "google_org_id" {
  default = "873117237046"
}
variable "google_billing_account" {
  default = "018859-87E2B2-3B11B8"
}
variable "google_project_id_prefix" {
  default = "vicpah-tf-admin"
}
variable "google_project_name" {
  default = "VicPAH Terraform Root Admin"
}
variable "google_region" {
  default = "australia-southeast1"
}


variable "gitlab_group_id" {
  default = "1553734"
}
variable "gitlab_token" {
}
