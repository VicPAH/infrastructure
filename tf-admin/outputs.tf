output "project_id" {
  value = google_project.project.project_id
}
output "state_bucket_name" {
  value = google_storage_bucket.state_backend.name
}
