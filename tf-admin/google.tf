# Project
resource "random_id" "id" {
  byte_length = 4
  prefix      = "${var.google_project_id_prefix}-"
}
resource "google_project" "project" {
  provider        = google.init
  name            = var.google_project_name
  project_id      = random_id.id.hex
  billing_account = var.google_billing_account
  org_id          = var.google_org_id
  labels = {
    id_prefix = var.google_project_id_prefix
  }
  lifecycle {
    prevent_destroy = true
  }
}
resource "google_project_service" "service" {
  for_each = toset([
    "billingbudgets.googleapis.com",
    "cloudbilling.googleapis.com",
    "cloudresourcemanager.googleapis.com",
    "iam.googleapis.com",
  ])

  service = each.key

  project            = google_project.project.project_id
  disable_on_destroy = false
}


# Bucket
resource "google_storage_bucket" "state_backend" {
  name               = "${google_project.project.project_id}-tfstate"
  location           = var.google_region
  bucket_policy_only = true
  lifecycle {
    prevent_destroy = true
  }
}
