data "google_projects" "root" {
  filter = "labels.id_prefix:${var.tf_project_prefix} lifecycleState:ACTIVE"
}


locals {
  root_project = data.google_projects.root.projects[0]
}


data "gitlab_group" "vicpah" {
  full_path = "VicPAH"
}
# data "gitlab_projects" "legacy_www_content" {
#   group_id = data.gitlab_group.vicpah.id
#   search   = "vicpah.gitlab.io-content"
# }
# data "gitlab_projects" "legacy_www_deploy" {
#   group_id = data.gitlab_group.vicpah.id
#   search   = "vicpah.gitlab.io-deploy"
# }
data "gitlab_projects" "legacy_www_react" {
  group_id = data.gitlab_group.vicpah.id
  search   = "vicpah.gitlab.io-react"
}
