resource "gitlab_group" "lib" {
  name             = "Libraries"
  path             = "lib"
  visibility_level = "public"
  parent_id        = gitlab_group.vicpah.id
}

# Global registry namespace
module "registry" {
  source = "./modules/gitlab_project"
  name   = "Registry"
  ns_id  = gitlab_group.vicpah.id
}

# Middleware
module "gclound_func_middleware_auth" {
  source              = "./modules/gitlab_lib_project"
  name                = "GCloud functions auth middleware"
  ns_id               = gitlab_group.lib.id
  registry_project_id = module.registry.project_id
}
module "gclound_func_middleware_cors" {
  source              = "./modules/gitlab_lib_project"
  name                = "GCloud functions CORS middleware"
  ns_id               = gitlab_group.lib.id
  registry_project_id = module.registry.project_id
}
module "gclound_func_middleware_express" {
  source              = "./modules/gitlab_lib_project"
  name                = "GCloud functions express.js middleware adapter"
  ns_id               = gitlab_group.lib.id
  registry_project_id = module.registry.project_id
}
module "gclound_func_middleware" {
  source              = "./modules/gitlab_lib_project"
  name                = "GCloud functions middleware core"
  ns_id               = gitlab_group.lib.id
  registry_project_id = module.registry.project_id
}
module "gclound_func_middleware_joi" {
  source              = "./modules/gitlab_lib_project"
  name                = "GCloud functions joi middleware"
  ns_id               = gitlab_group.lib.id
  registry_project_id = module.registry.project_id
}
module "gclound_func_middleware_method" {
  source              = "./modules/gitlab_lib_project"
  name                = "GCloud functions HTTP method middleware"
  ns_id               = gitlab_group.lib.id
  registry_project_id = module.registry.project_id
}
module "gclound_func_middleware_path" {
  source              = "./modules/gitlab_lib_project"
  name                = "GCloud functions URL path parse middleware"
  ns_id               = gitlab_group.lib.id
  registry_project_id = module.registry.project_id
}
module "gclound_func_middleware_error" {
  source              = "./modules/gitlab_lib_project"
  name                = "GCloud functions HTTP errors middleware"
  ns_id               = gitlab_group.lib.id
  registry_project_id = module.registry.project_id
}

# General
module "jwt_utils" {
  source              = "./modules/gitlab_lib_project"
  name                = "JWT Utils"
  ns_id               = gitlab_group.lib.id
  registry_project_id = module.registry.project_id
}
