variable "name" {}
variable "ns_id" {}
variable "tfplan_pass" {
  type    = bool
  default = true
}
variable "container_registry_enabled" {
  type    = bool
  default = false
}
