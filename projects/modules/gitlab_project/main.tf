resource "gitlab_project" "main" {
  name             = var.name
  namespace_id     = var.ns_id
  visibility_level = "public"
  default_branch   = "master"

  container_registry_enabled                       = var.container_registry_enabled
  only_allow_merge_if_all_discussions_are_resolved = true
  only_allow_merge_if_pipeline_succeeds            = true
  shared_runners_enabled                           = true
  snippets_enabled                                 = false
  wiki_enabled                                     = false
}
resource "gitlab_branch_protection" "master" {
  project = gitlab_project.main.id
  branch  = "master"
  # no one, guest, reporter, developer, maintainer, owner, master
  push_access_level  = "maintainer"
  merge_access_level = "maintainer"
}


module "tfplan_pass" {
  source        = "../../../tf_modules/gitlab_secret_var"
  create        = var.tfplan_pass
  project       = gitlab_project.main.id
  name          = "tfplan_pass"
  variable_type = "file"
}
