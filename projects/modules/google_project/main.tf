locals {
  id_prefix = join("-", split(" ", lower(var.name)))
}

resource "random_id" "id" {
  byte_length = 2
  prefix      = "${local.id_prefix}-"
}

resource "google_folder" "main" {
  display_name = var.name
  parent       = var.parent
}
resource "google_project" "main" {
  for_each        = toset(var.envs)
  name            = "${var.name} ${upper(each.value)}"
  project_id      = "${random_id.id.hex}-${lower(each.value)}"
  folder_id       = google_folder.main.name
  billing_account = var.billing_account
  labels = merge(var.labels, {
    id_prefix = local.id_prefix
    env       = lower(each.value)
  })
}
resource "google_project_iam_member" "gitlab" {
  for_each = google_project.main
  project  = each.value.project_id
  member   = "serviceAccount:${var.sa_email}"
  role     = "roles/owner"
}
resource "google_project_iam_member" "biru" {
  for_each = google_project.main
  project  = each.value.project_id
  member   = "user:biru@vicpah.org.au"
  role     = "roles/owner"
}

resource "google_project_service" "serviceusage" {
  for_each           = google_project.main
  service            = "serviceusage.googleapis.com"
  project            = each.value.project_id
  disable_on_destroy = false
}
resource "google_project_service" "cloudresourcemanager" {
  for_each           = google_project.main
  service            = "cloudresourcemanager.googleapis.com"
  project            = each.value.project_id
  disable_on_destroy = false
}
