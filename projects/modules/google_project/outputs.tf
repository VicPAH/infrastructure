output "folder_id" {
  value = google_folder.main.id
}
output "project_ids" {
  value = {
    for k in var.envs :
    k => google_project.main[k].project_id
  }
}
output "project_id_prefix" {
  value = random_id.id.hex
}
