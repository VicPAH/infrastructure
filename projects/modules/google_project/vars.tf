variable "name" {}
variable "envs" {
  default = ["dev", "prod"]
}
variable "labels" {
  default = {}
}
variable "parent" {}
variable "sa_email" {}
variable "billing_account" {}
