
module "gitlab" {
  source                     = "../gitlab_project"
  name                       = var.name
  ns_id                      = var.gitlab_ns_id
  container_registry_enabled = var.gitlab_container_registry_enabled
}
module "google" {
  source          = "../google_project"
  name            = var.name
  envs            = var.envs
  labels          = var.labels
  parent          = var.google_parent
  sa_email        = var.google_sa_email
  billing_account = var.google_billing_account
}


module "gitlab_google_sa" {
  source             = "../gitlab_google_sa"
  project_name       = var.name
  google_project_ids = module.google.project_ids
  gitlab_project_id  = module.gitlab.project_id
  tf_project_id      = var.tf_project_id
  tf_global_role_id  = var.tf_global_role_id
  tf_backend_bucket  = var.tf_backend_bucket
}


module "gitlab_aws_iam_user" {
  count             = var.aws_iam_user ? 1 : 0
  source            = "../gitlab_aws_iam_user"
  project_name      = var.name
  gitlab_project_id = module.gitlab.project_id
}


resource "gitlab_project_variable" "google_project_id_prefix" {
  project           = module.gitlab.project_id
  key               = "TF_VAR_google_project_id_prefix"
  value             = module.google.project_id_prefix
  protected         = false
  environment_scope = "*"
}
