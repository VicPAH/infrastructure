variable "name" {}


variable "envs" {
  default = ["dev", "prod"]
}
variable "labels" {
  default = {}
}
variable "google_parent" {}
variable "google_sa_email" {}
variable "google_billing_account" {}


variable "gitlab_ns_id" {}
variable "gitlab_container_registry_enabled" {
  default = false
}


variable "aws_iam_user" {
  default = false
}


variable "tf_backend_bucket" {}
variable "tf_project_id" {}
variable "tf_global_role_id" {}
