output "google_folder_id" {
  value = module.google.folder_id
}
output "google_project_ids" {
  value = module.google.project_ids
}
output "google_project_id_prefix" {
  value = module.google.project_id_prefix
}

output "gitlab_project_id" {
  value = module.gitlab.project_id
}
