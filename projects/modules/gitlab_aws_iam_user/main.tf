locals {
  id_prefix = join("-", split(" ", lower(var.project_name)))
}


resource "aws_iam_user" "gitlab" {
  name = "gitlab-${local.id_prefix}-20210220"
  path = "/cicd/"
}
resource "aws_iam_user_policy_attachment" "admin" {
  user       = aws_iam_user.gitlab.name
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}
resource "aws_iam_access_key" "gitlab" {
  user = aws_iam_user.gitlab.name
}


resource "gitlab_project_variable" "key_id" {
  project           = var.gitlab_project_id
  key               = "AWS_ACCESS_KEY_ID"
  value             = aws_iam_access_key.gitlab.id
  protected         = true
  environment_scope = "*"
}
resource "gitlab_project_variable" "key_secret" {
  project           = var.gitlab_project_id
  key               = "AWS_SECRET_ACCESS_KEY"
  value             = aws_iam_access_key.gitlab.secret
  protected         = true
  masked            = true
  environment_scope = "*"
}
