terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
    }
    aws = {
      source = "hashicorp/aws"
    }
  }
  required_version = ">= 0.13"
}
