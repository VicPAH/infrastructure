variable "project_id" {
  type = number
}
variable "environment_scope" {
  type = string
}
variable "values" {
  type = map(string)
}
