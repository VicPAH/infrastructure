# resource "gitlab_project_variable" "vars" {
#   for_each          = var.values
#   key               = each.key
#   value             = each.value
#   project           = var.project_id
#   environment_scope = var.environment_scope
#   protected         = false
# }

# hack until https://github.com/gitlabhq/terraform-provider-gitlab/issues/213
locals {
  env_name = var.environment_scope == "*" ? "dev" : var.environment_scope
}

resource "gitlab_project_variable" "vars_hack" {
  key = "TF_CIVARS_HACK_${local.env_name}"
  value = join(
    "\n",
    [for k in keys(var.values) : "${k}=\"${var.values[k]}\""],
  )
  project = var.project_id
  # environment_scope = var.environment_scope
  environment_scope = "*"
  protected         = false
  variable_type     = "file"
}
