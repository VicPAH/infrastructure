module "project" {
  source      = "../gitlab_project"
  name        = var.name
  ns_id       = var.ns_id
  tfplan_pass = false
}
resource "gitlab_project_variable" "registry_project_id" {
  key               = "REGISTRY_PROJECT_ID"
  value             = var.registry_project_id
  project           = module.project.project_id
  environment_scope = "*"
  protected         = false
}
