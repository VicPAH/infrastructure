module "env" {
  source            = "../www_civars_env"
  for_each          = var.values
  project_id        = var.project_id
  environment_scope = each.key
  values            = each.value
}
