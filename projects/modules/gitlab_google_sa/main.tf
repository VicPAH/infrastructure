locals {
  id_prefix = join("-", split(" ", lower(var.project_name)))
}

resource "google_service_account" "gitlab" {
  account_id   = "gitlab-20210220"
  display_name = "${var.project_name} GitLab"
  project      = var.google_project_ids.prod
}
resource "google_service_account_key" "gitlab" {
  service_account_id = google_service_account.gitlab.name
}
resource "google_project_iam_member" "gitlab" {
  for_each = var.google_project_ids
  project  = each.value
  member   = "serviceAccount:${google_service_account.gitlab.email}"
  role     = "roles/owner"
}
resource "google_project_iam_member" "gitlab_tf_global" {
  project = var.tf_project_id
  member  = "serviceAccount:${google_service_account.gitlab.email}"
  role    = var.tf_global_role_id
}
resource "google_project_iam_member" "gitlab_tf_prefixed" {
  project = var.tf_project_id
  member  = "serviceAccount:${google_service_account.gitlab.email}"
  role    = "roles/storage.admin"
  condition {
    title      = "project prefix only"
    expression = "resource.name.startsWith(\"projects/_/buckets/${var.tf_backend_bucket}/objects/${local.id_prefix}/\")"
  }
}


resource "gitlab_project_variable" "google_sa_private" {
  project           = var.gitlab_project_id
  key               = "GOOGLE_APPLICATION_CREDENTIALS"
  value             = base64decode(google_service_account_key.gitlab.private_key)
  variable_type     = "file"
  protected         = true
  environment_scope = "*"
}
