variable "project_name" {}

variable "google_project_ids" {}
variable "gitlab_project_id" {}

variable "tf_backend_bucket" {}
variable "tf_project_id" {}
variable "tf_global_role_id" {}
