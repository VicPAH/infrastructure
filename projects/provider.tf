terraform {
  backend "gcs" {
  }
}
provider "random" {
}
provider "google" {
  region = var.region
}
provider "gitlab" {
  token = var.gitlab_token
}
provider "aws" {
}
