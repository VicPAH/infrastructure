terraform {
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 3.5"
    }
    google = {
      source  = "hashicorp/google"
      version = "~> 3.31"
    }
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.29"
    }
    random = {
      source  = "hashicorp/random"
      version = "~> 2.3"
    }
  }
  required_version = ">= 0.13"
}
