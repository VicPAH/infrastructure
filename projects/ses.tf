data "aws_route53_zone" "vicpah_org_au" {
  name = "vicpah.org.au."
}

# Identity
resource "aws_ses_domain_identity" "vicpah_org_au" {
  domain = "vicpah.org.au"
}
resource "aws_route53_record" "vicpah_org_au_ses" {
  zone_id = data.aws_route53_zone.vicpah_org_au.zone_id
  name    = "_amazonses.${aws_ses_domain_identity.vicpah_org_au.id}"
  type    = "TXT"
  ttl     = "600"
  records = [aws_ses_domain_identity.vicpah_org_au.verification_token]
}
resource "aws_ses_domain_identity_verification" "vicpah_org_au" {
  domain     = aws_ses_domain_identity.vicpah_org_au.id
  depends_on = [aws_route53_record.vicpah_org_au_ses]
}

# DKIM
resource "aws_ses_domain_dkim" "vicpah_org_au" {
  domain = aws_ses_domain_identity.vicpah_org_au.domain
  depends_on = [aws_ses_domain_identity_verification.vicpah_org_au]
}
resource "aws_route53_record" "vicpah_org_au_dkim" {
  count   = 3
  zone_id = data.aws_route53_zone.vicpah_org_au.zone_id
  name    = "${element(aws_ses_domain_dkim.vicpah_org_au.dkim_tokens, count.index)}._domainkey"
  type    = "CNAME"
  ttl     = "600"
  records = ["${element(aws_ses_domain_dkim.vicpah_org_au.dkim_tokens, count.index)}.dkim.amazonses.com"]
}

# SPF
resource "aws_route53_record" "vicpah_org_au_spf" {
  zone_id = data.aws_route53_zone.vicpah_org_au.zone_id
  name    = aws_ses_domain_identity.vicpah_org_au.id
  type    = "SPF"
  ttl     = "600"
  records = ["v=spf1 include:_spf.google.com include:amazonses.com ~all"]
}
