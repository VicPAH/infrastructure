variable "google_org_id" {}
variable "google_billing_account" {}
variable "google_sa_email" {}
variable "region" {
  default = "australia-southeast1"
}

variable "gitlab_token" {}

variable "tf_backend_bucket" {}
variable "tf_project_prefix" {
  default = "vicpah-tf-admin"
}
