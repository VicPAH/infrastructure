module "www_civars" {
  source = "./modules/www_civars"
  # project_id = module.www_react.project_id
  project_id = data.gitlab_projects.legacy_www_react.projects[0].id
  values = {
    "*" = {
      APPLE_MAPS_URL             = "https://australia-southeast1-${module.apple_maps.google_project_id_prefix}-dev.cloudfunctions.net",
      ICAL_HOST_REPLACE_DEF      = "vicpah.org.au,www.vicpah.org.au,vicpah.com.au,www.vicpah.com.au,vicpah.gitlab.io",
      MEMBER_PROFILES_URL        = "https://australia-southeast1-${module.member_profiles.google_project_id_prefix}-dev.cloudfunctions.net",
      PHOTO_COMP_2020_API_URL    = "https://9hq1gfabt8.execute-api.ap-southeast-2.amazonaws.com/dev",
      PHOTO_COMP_2020_STATIC_URL = "photo-comp-dev-photo-output-lwqhetubhoehtqwovjk.s3.amazonaws.com",
    },
    prod = {
      APPLE_MAPS_URL             = "https://australia-southeast1-${module.apple_maps.google_project_id_prefix}-prod.cloudfunctions.net",
      MEMBER_PROFILES_URL        = "https://australia-southeast1-${module.member_profiles.google_project_id_prefix}-prod.cloudfunctions.net",
      PHOTO_COMP_2020_API_URL    = "https://666n4cgo20.execute-api.ap-southeast-2.amazonaws.com/prod",
      PHOTO_COMP_2020_STATIC_URL = "photo-comp-prod-photo-output-lwqhetubhoehtqwovjk.s3.amazonaws.com",
      SENTRY_DSN                 = "https://a18ce20d74584b77890649eebadf26ed@o376672.ingest.sentry.io/5197701",
    },
  }
}
