resource "google_folder" "api" {
  display_name = "api"
  parent       = "organizations/${var.google_org_id}"
}
resource "gitlab_group" "vicpah" {
  name             = "VicPAH"
  path             = "VicPAH"
  visibility_level = "public"
}


resource "gitlab_group_variable" "dev_recaptcha_site_key" {
  group     = gitlab_group.vicpah.id
  key       = "DEV_RECAPTCHA_SITE_KEY"
  value     = "6Lf9CcIZAAAAAE68g2Dm4Qdi_KHSZhzcnVB7SUJY"
  protected = false
  masked    = false
}
resource "gitlab_group_variable" "prod_recaptcha_site_key" {
  group     = gitlab_group.vicpah.id
  key       = "PROD_RECAPTCHA_SITE_KEY"
  value     = "6LcECsIZAAAAAEANDHoNfpzA6JZNHMtIGiQbH1wJ"
  protected = false
  masked    = false
}
resource "gitlab_group_variable" "aws_default_region" {
  group     = gitlab_group.vicpah.id
  key       = "AWS_DEFAULT_REGION"
  value     = "ap-southeast-2"
  protected = false
  masked    = false
}


resource "google_project_iam_custom_role" "terraform_global" {
  project     = local.root_project.project_id
  role_id     = "tf_global"
  title       = "Terraform Global"
  description = "Unrestricted permissions for TF service accounts"
  permissions = ["storage.objects.list"]
}
