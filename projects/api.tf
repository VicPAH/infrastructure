resource "gitlab_group" "api" {
  name             = "API"
  path             = "API"
  visibility_level = "public"
  parent_id        = gitlab_group.vicpah.id
}


module "member_profiles" {
  source                            = "./modules/project"
  name                              = "Member Profiles"
  google_parent                     = google_folder.api.name
  gitlab_ns_id                      = gitlab_group.api.id
  gitlab_container_registry_enabled = true
  google_sa_email                   = var.google_sa_email
  google_billing_account            = var.google_billing_account
  aws_iam_user                      = true
  tf_backend_bucket                 = var.tf_backend_bucket
  tf_project_id                     = local.root_project.project_id
  tf_global_role_id                 = google_project_iam_custom_role.terraform_global.id
}
resource "google_project_service" "member_profiles_cloudbuild" {
  for_each           = module.member_profiles.google_project_ids
  service            = "cloudbuild.googleapis.com"
  project            = each.value
  disable_on_destroy = false
}


module "apple_maps" {
  source                 = "./modules/project"
  name                   = "Apple Maps"
  google_parent          = google_folder.api.name
  gitlab_ns_id           = gitlab_group.api.id
  google_sa_email        = var.google_sa_email
  google_billing_account = var.google_billing_account
  tf_backend_bucket      = var.tf_backend_bucket
  tf_project_id          = local.root_project.project_id
  tf_global_role_id      = google_project_iam_custom_role.terraform_global.id
}


module "photo_comp" {
  source                 = "./modules/project"
  name                   = "Photo Comp"
  google_parent          = google_folder.api.name
  gitlab_ns_id           = gitlab_group.api.id
  google_sa_email        = var.google_sa_email
  google_billing_account = var.google_billing_account
  tf_backend_bucket      = var.tf_backend_bucket
  tf_project_id          = local.root_project.project_id
  tf_global_role_id      = google_project_iam_custom_role.terraform_global.id
}


module "polls" {
  source                            = "./modules/project"
  name                              = "Polls"
  google_parent                     = google_folder.api.name
  gitlab_ns_id                      = gitlab_group.api.id
  gitlab_container_registry_enabled = true
  google_sa_email                   = var.google_sa_email
  google_billing_account            = var.google_billing_account
  tf_backend_bucket                 = var.tf_backend_bucket
  tf_project_id                     = local.root_project.project_id
  tf_global_role_id                 = google_project_iam_custom_role.terraform_global.id
}


# Add member profiles prefix var for bad service discovery
locals {
  member_profiles_var_project_ids = [
    module.polls.gitlab_project_id,
  ]
}
resource "gitlab_project_variable" "member_profiles" {
  count             = length(local.member_profiles_var_project_ids)
  project           = local.member_profiles_var_project_ids[count.index]
  key               = "TF_VAR_member_profiles_google_project_id_prefix"
  value             = module.member_profiles.google_project_id_prefix
  protected         = false
  environment_scope = "*"
}


# Set the default deploy env
locals {
  default_env_project_ids = [
    module.apple_maps.gitlab_project_id,
    module.photo_comp.gitlab_project_id,
  ]
}
resource "gitlab_project_variable" "env" {
  count             = length(local.default_env_project_ids)
  project           = local.default_env_project_ids[count.index]
  key               = "TF_VAR_env"
  value             = "dev"
  protected         = false
  environment_scope = "*"
}
