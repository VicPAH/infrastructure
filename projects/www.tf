resource "gitlab_group" "www" {
  name             = "WWW"
  path             = "WWW"
  visibility_level = "public"
  parent_id        = gitlab_group.vicpah.id
}
module "www_react" {
  source = "./modules/gitlab_project"
  name   = "React"
  ns_id  = gitlab_group.www.id
}
module "www_content" {
  source = "./modules/gitlab_project"
  name   = "Content"
  ns_id  = gitlab_group.www.id
}
