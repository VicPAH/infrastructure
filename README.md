# VicPAH Infrastructure Management

- **tf-admin**: GCP project and GCS bucket for Terraform state. This uses local
  state, and thus *must store no secrets*
- **tf-admin-service-accounts**: Service accounts that allow the infrastructure
  automation to run in GitLab CI. This also manages the Infrastructure repo
- **projects**: Setup of Google/GitLab projects, folders, basic permissions, etc

## CI

Terraform plan/apply for tf-admin-service-accounts, and projects is done
through CI. The `terraform plan` file is encrypted (it contains secrets),
stored as a job artifact, downloaded by the deploy stage, decrypted, and then
used to run the `terraform apply` so that you can be sure that the plan outputs
all changes that will be made.
